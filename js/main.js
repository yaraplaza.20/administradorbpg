
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
  "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

  import { getStorage, ref, uploadBytesResumable, getDownloadURL }
  from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";


  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyARz05od1gKpl9GaZE8nL56v6yW1u2QLWE",
    authDomain: "proyectofinalhp.firebaseapp.com",
    projectId: "proyectofinalhp",
    storageBucket: "proyectofinalhp.appspot.com",
    messagingSenderId: "649610069711",
    appId: "1:649610069711:web:53bc61b4c79c1d07f619ba"
  };
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase(app);
  const storage = getStorage();

// variables para el manejo de la imagen
const imageInput = document.getElementById ('imageInput');
const uploadButton = document.getElementById ('uploadButton');
const progressDiv = document.getElementById ('progress');
const txtUrlInput = document.getElementById ('txtUrl');

// Declarar unas variables global
var numCodigo = 0;
var nombre = "";
var grupo = "";
var descrippcion = "";
var urlImag = "";

// Funciones
function leerInputs()
{
    numCodigo = document.getElementById('txtNumCodigo').value;
    nombre = document.getElementById('txtNombre').value;
    grupo = document.getElementById('txtGrupo').value;
    descrippcion = document.getElementById('txtDescripcion').value;
    urlImag = document.getElementById('txtUrl').value;
}

function mostrarMensaje (mensaje)
{
    var mensajeElement = document.getElementById ('mensaje');
    mensajeElement.textContent = mensaje;
    mensajeElement.style.display = 'block';
    setTimeout(() => {
    mensajeElement.style.display = 'none'    
    }, 1000);
}

const btnAgregar = document.getElementById ('btnAgregar');
btnAgregar.addEventListener ('click',insertarProducto);

function insertarProducto ()
{
    alert ("Ingrese a add db");
    leerInputs();
    // Validar
    if (numCodigo === "" || nombre === "" || grupo === "" || descrippcion === "")
    {
        mostrarMensaje ("Faltaron datos por capturar");
        return;
    }

    // Funcion de firebase para agregar registro

    set (
        refS (db, 'Productos/' + numCodigo),
        {
            // Datos a guardar realizar json con los campos y datos de la tabla campo:valor

            numCodigo: numCodigo,
            nombre:nombre,
            grupo:grupo,
            descrippcion:descrippcion,
            urlImag:urlImag,
        }

    ).then(()=>{
        alert("Se agregó con exito");
    } ).catch ((error)=>{
        alert ("Ocurrió un error");
    });
}

function limpiarInputs()
{
    document.getElementById('txtNumCodigo').value = '';
    document.getElementById('txtNombre').value = '';
    document.getElementById('txtGrupo').value = '';
    document.getElementById('txtDescripcion').value = '';
    document.getElementById('txtUrl').value = '';
}

function escribirInputs()
{
    document.getElementById('txtNombre').value = nombre;
    document.getElementById('txtGrupo').value = grupo;
    document.getElementById('txtDescripcion').value = descrippcion;
    document.getElementById('txtUrl').value = urlImag;
}

function buscarProducto()
{
    const numCodigo = document.getElementById ('txtNumCodigo').value.trim();
    if (numCodigo === "")
    {
        mostrarMensaje ("No se ingresó Codigo");
        return;
    }

    const dbref = refS(db);
    get (child(dbref, 'Productos/' + numCodigo)).then((snapshot)=>
    {
        if (snapshot.exists())
        {
            // const{marca,modelo,descrippcion,urlImage}= snapshot.val();
            // escribirInputs (numSerie,marca,modelo,descrippcion,urlImage);
            
            nombre = snapshot.val().nombre;
            grupo = snapshot.val().grupo;
            descrippcion = snapshot.val().descrippcion;
            urlImag = snapshot.val().urlImag;
            escribirInputs();
        }
        else {
            limpiarInputs();
            mostrarMensaje("El producto con codigo" + numCodigo + "no existe.");
        }
    });
}

btnBuscar.addEventListener ('click', buscarProducto);

Listarproductos();

function Listarproductos()
{
    const dbref = refS (db, 'Productos');
    const tabla = document.getElementById ('tablaProductos')
    const tbody = tabla.querySelector('tbody');

    tbody.innerHTML = '';

    onValue (dbref, (snapshot)=>{
    snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;

        const data = childSnapshot.val();
        var fila = document.createElement('tr');

        var celdaCodigo = document.createElement('td');
        celdaCodigo.textContent = childKey;
        fila.appendChild(celdaCodigo);

        var celdaNombre = document.createElement('td');
        celdaNombre.textContent = data.nombre;
        fila.appendChild(celdaNombre);

        var celdaGrupo = document.createElement('td');
        celdaGrupo.textContent = data.grupo;
        fila.appendChild(celdaGrupo);

        var celdaCantidad = document.createElement('td');
        celdaCantidad.textContent = data.descrippcion;
        fila.appendChild(celdaCantidad);

        var celdaImagen = document.createElement ('td');
        var imagen = document.createElement('img');
        imagen.src = data.urlImag;
        imagen.width =100;
        celdaImagen.appendChild(imagen);
        fila.appendChild(celdaImagen);
        tbody.appendChild(fila);
    });
    }, {onlyOnce:true});
}

// Listarproductos();

function actualizarProducto()
{
    leerInputs();
    if (numCodigo === "" || nombre=== "" || grupo === "" || descrippcion === "")
    {
        mostrarMensaje ("Favor de capturar toda la informacion");
        return;
    }
    alert("Actualizar");
    update(refS(db,'Productos/'+numCodigo),
    {
        numCodigo:numCodigo,
        nombre:nombre,
        grupo:grupo,
        descrippcion:descrippcion,
        urlImag:urlImag,
    }),then(()=>{
        mostrarMensaje ("Se actualizó con exito");
        limpiarInputs();
        Listarproductos();
    }).catch((error)=>{
        mostrarMensaje("Ocurrió un error:"+ error);
    });
// Listarproductos();
}

function eliminarProducto()
{
    let numCodigo = document.getElementById('txtNumCodigo').value.trim();
    if (numCodigo === "")
    {
        mostrarMensaje ("No se ingresó un codigo valido");
        return;
    }

    const dbref = refS(db);
    get (child(dbref, 'Productos/'+ numCodigo))
    .then((snapshot)=>
    { if (snapshot.exists())
        {
            remove(refS(db, 'Productos/' + numCodigo))
            .then(() => {
        mostrarMensaje ("Producto eliminado con exito");
        limpiarInputs();
        Listarproductos();
    })
    .catch((error) =>
    {
        mostrarMensaje ("Ocurrio un error al eliminar el producto");
    });

}else{
    limpiarInputs();
    mostrarMensaje ("El producto con ID" + numCodigo + "no existe.");
}
});
Listarproductos();
}


const btnActualizar = document.getElementById ('btnActualizar');
btnActualizar.addEventListener ('click', actualizarProducto);

const btnBorrar = document.getElementById ('btnBorrar');
btnBorrar.addEventListener('click', eliminarProducto);


uploadButton.addEventListener('click',  (event) => {
    event.preventDefault();
    const file = imageInput.files[0];

    if (file) {
        const storagRef = ref (storage, file.name);
        const uploadTask = uploadBytesResumable ( storagRef, file);
        uploadTask.on ('state_changed', (snapshot) =>{
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes)*100;
            progressDiv.textContent = 'Progreso '+ progress.toFixed (2) + '%';
        }, (error) =>{
            console.error (error);
        }, () => {
            getDownloadURL ( uploadTask.snapshot.ref).then ((downloadURL) => {
                txtUrlInput.value = downloadURL;
                setTimeout(() => {
                    progressDiv.textContent = '';
                },500);
            }) .catch ((error) => {
                console.error (error);
            });
        });
    }
});

